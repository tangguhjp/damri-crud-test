"use strict";

var listSchedule = (function () {
    return {
        init: function () {
            loadTableSchedule();

            $('#btn-add-shedule').click(function(){
                newSchedule();
            })

            $('.cities').select2();

            $('#tanggal').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mm-yyyy'
            });

            $('#jam').timepicker({
                uiLibrary: 'bootstrap4'
            });

            $("#form-schedule").submit(function( e ) {
                e.preventDefault();
                var url = 'schedule/'+($('#form-schedule input[name="id"]').length ? 'update_schedule' : 'add_schedule')
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: $(this).serializeArray(),
                    dataType: 'JSON',
                    success: function(response){
                        if(response.status){
                            $('#modal_schedule').modal('hide');
                            swal({
                                title: 'Success',
                                text: response.message,
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: false
                            })
                            $('#schedule-table').DataTable().ajax.reload(null, false);
                        }else{
                            swal({
                                title: 'Failed',
                                text: response.message,
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: false
                            })
                        }
                    }
                });

            });
        }
    };
})();


function loadTableSchedule(){
    var tabel = $('#schedule-table').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true,
        "order": [
            [0, 'asc']
        ],
        "ajax": {
            url: "schedule/data_list",
            type: "GET",
            "dataSrc": ""
        },
        "columns": [{
                data: null
            },
            {
                data: "kota_asal"
            },
            {
                data: "kota_tujuan"
            },
            {
                data: "tgl_jadwal"
            },
            {
                data: "hrg_tiket",
                render : function(data){
                    return data;
                }
            },
            {
                data: "seat",
                render : function(data){
                    return data;
                }
            },
            {
                data: "jam_berangkat"
            },
            {
                data: "active",
                render : function(data, type, row, meta){
                    return '<div class="list-icons">\
                        <div class="dropdown">\
                            <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false">\
                                <i class="icon-menu9"></i>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">\
                                <a href="#" class="dropdown-item" onClick="editSchedule('+row.id+')"><i class="fa fa-pencil-square-o"></i>Edit</a>\
                                <a href="#" class="dropdown-item" onClick="removeSchedule('+row.id+')"><i class="fa fa-trash"></i>Hapus</a>\
                            </div>\
                        </div>\
                    </div>';
                }
            },
        ],
        "columnDefs": [
            {"className": "dt-center", "targets": [0,3,5,6,7]}
        ]
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function newSchedule(){
    $('#form-schedule input[name="id"]').remove();
    $('#form-schedule')[0].reset();
    $("#form-schedule select").val('').trigger('change');
    $('#modal_schedule').modal('toggle');
    $('#modal_schedule').modal('show');
}

function editSchedule(id){
    $.ajax({
        url: 'schedule/detail/'+id,
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            if(response.status){
                var schedule = response.data;
                $('#modal_schedule').modal('toggle');
                $('#modal_schedule').modal('show');
                $('#modal_schedule select[name="asal"]').val(schedule.asal).trigger('change');
                $('#modal_schedule select[name="tujuan"]').val(schedule.tujuan).trigger('change');
                $('#modal_schedule input[name="seat"]').val(schedule.seat);
                $('#modal_schedule input[name="harga"]').val(schedule.harga);
                $('#modal_schedule input[name="tanggal"]').val(schedule.tgl);
                $('#modal_schedule input[name="jam"]').val(schedule.jam);
                $('#form-schedule').append("<input type='hidden' name='id' value="+schedule.id+">");
            }
        }
    });
}

function removeSchedule(id){
    swal({
        title: 'Apakah anda yakin?',
        text: "Data akan terhapus permanen",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function (result) {
        if (result.dismiss == 'cancel') {
            swal({
                title: 'Cancelled',
                text: "Tidak ada jadwal tiket yang terhapus",
                type: 'error',
                timer: 1500,
                showConfirmButton: false
            })
        }else{
            $.ajax({
                url: 'schedule/remove',
                type: 'POST',
                data: {
                    id : id
                },
                dataType: 'JSON',
                success: function(response){
                    if(response.status){
                        $('#modal_schedule').modal('hide');
                        swal({
                            title: 'Success',
                            text: response.message,
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: false
                        })
                        $('#schedule-table').DataTable().ajax.reload(null, false);
                    }else{
                        swal({
                            title: 'Failed',
                            text: response.message,
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: false
                        })
                    }
                }
            });
        }
    });
}

jQuery(document).ready(function () {
    listSchedule.init();
});