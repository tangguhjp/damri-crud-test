"use strict";

var listSchedule = (function () {
    return {
        init: function () {
            loadTableSchedule();
            // $("#grade").select2({
            //     placeholder: "--Pilih Grade--",
            // });
            // $('#grade option:eq(1)').attr('selected','selected');
            // $("#grade").trigger("change");

            // $("#kategori").select2({
            //     placeholder: "",
            // });
            // $("#job-fit-family").select2({
            //     placeholder: "",
            //     width: "100%",
            // });
            // $('#job-fit-family option:eq(1)').attr('selected','selected');
            // $("#job-fit-family").trigger("change");

            // $("#txt-tahun").datepicker({
            //     minViewMode: 2,
            //     format: 'yyyy',
            //     autoclose: true
            // });
        }
    };
})();


function loadTableSchedule(){
    var tabel = $('#schedule-table').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        // "data":data, // Combobox Limit
        "ajax": {
            url: "schedule/data_list",
            type: "GET",
            // data:{
            //     kompartemen : $("#kompartemen").val(),
            //     departemen : $("#departemen").val(),
            //     tahun : $('#txt-tahun').val()
            // },
            "dataSrc": ""
        },
        "columns": [{
                data: null
            },
            {
                data: "kota_asal"
            },
            {
                data: "kota_tujuan"
            },
            {
                data: "tgl_jadwal"
            },
            {
                data: "hrg_tiket",
                render : function(data){
                    return data;
                }
            },
            {
                data: "seat",
                render : function(data){
                    return data;
                }
            },
            {
                data: "jam_berangkat"
            },
            // {
            //     data: "active",
            //     render : function(data){
            //         if(data){
            //             return "<button class='btn btn-primary' onclick='modalPDF(`"+data+"`)'><i class='far fa-file-pdf'></i> pdf</button>";
            //         }
            //         return "-";
            //     }
            // },
        ],
        "columnDefs": [
            {"className": "dt-center", "targets": [0,3,5,6]}
        ],
        createdRow: function(row, data, index) {
            // $("td", row)
            //     .eq(0)
            //     .addClass("text-center");
            // $("td", row)
            //     .eq(3)
            //     .addClass("text-center");
            // $("td", row)
            //     .eq(5)
            //     .addClass("text-center");
        }
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

jQuery(document).ready(function () {
    listSchedule.init();
});