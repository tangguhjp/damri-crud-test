<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Dompdf\Dompdf;
use Dompdf\Options;

class Schedule extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('string');
		$this->load->library(['ion_auth', 'session']);
		$this->load->model('schedule_model');

		if(!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
			
	}

	private function _render($view, $data = array())
	{
		$data['title'] = "Dashboard | DAMRI";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

    public function index()
	{
		$user = $this->ion_auth->user()->row()->username;
		if($this->ion_auth->is_admin()){
			$data['cities'] = $this->schedule_model->get_cities();
			$this->_render('schedule_list_admin', $data);
		}else{
			$this->_render('schedule_list', []);
		}
	}

	public function data_list(){
		$result = $this->schedule_model->get_schedule_data();
		echo json_encode($result);
	}

	public function detail($id){
		$result = [];
		if($this->ion_auth->is_admin()){
			$result = $this->schedule_model->get_detail($id);
		}
		echo json_encode($result);
	}

	public function remove(){
		$result = [];
		if($this->ion_auth->is_admin()){
			$id = $this->input->post('id');
			$result = $this->schedule_model->delete($id);
		}
		
		echo json_encode($result);
	}

	public function add_schedule(){
		$result = [];
		if($this->ion_auth->is_admin()){
			$result = $this->schedule_model->add($this->input->post());
		}
		echo json_encode($result);
	}

	public function update_schedule(){
		$result = [];
		if($this->ion_auth->is_admin()){
			$result = $this->schedule_model->update($this->input->post());
		}
		echo json_encode($result);
	}

	public function exportExcel(){
		$result = [];
		if($this->ion_auth->is_admin()){
			$schedules = $this->schedule_model->get_schedule_data();

			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setCellValue('A1', 'No');
			$sheet->setCellValue('B1', 'Kota Asal');
			$sheet->setCellValue('C1', 'Kota Tujuan');
			$sheet->setCellValue('D1', 'Tanggal');
			$sheet->setCellValue('E1', 'Harga');
			$sheet->setCellValue('F1', 'Seat');
			$sheet->getStyle("A1:F1")->getBorders()->getOutline()->setBorderStyle("thin");

			$ri = 2;
			foreach($schedules as $key => $value)
			{
				$sheet->setCellValue('A'.$ri, $ri-1);
				$sheet->setCellValue('B'.$ri, $value->kota_asal);
				$sheet->setCellValue('C'.$ri, $value->kota_tujuan);
				$sheet->setCellValue('D'.$ri, $value->tgl_jadwal);
				$sheet->setCellValue('E'.$ri, $value->hrg_tiket);
				$sheet->setCellValue('F'.$ri, $value->seat);
				$sheet->getStyle("A$ri:F$ri")->getBorders()->getOutline()->setBorderStyle("thin");
				$ri++;
			}

			for ($i = 'A'; $i !=  $spreadsheet->getActiveSheet()->getHighestColumn(); $i++) {
				$spreadsheet->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
			}
			$sheet->getStyle('A:F')->getAlignment()->setHorizontal('center');


			$writer = new Xlsx($spreadsheet);
			$filename = time().'_Laporan_Jadwal_Tiket';
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
			header('Cache-Control: max-age=0');
	
			$writer->save('php://output');
		}else{
			redirect('schedule');
		}
	}

	public function exportPdf(){
		$result = [];
		if($this->ion_auth->is_admin()){
			$stream = true;
			$filename = time().'_Laporan_Jadwal_Tiket';

			$data['schedules'] = $this->schedule_model->get_schedule_data();
			$html = $this->load->view('pdf_template',$data, true);
				
			$options = new Options();
			$options->set('isRemoteEnabled', TRUE);
			$dompdf = new Dompdf($options);
			$dompdf->loadHtml($html);
			$dompdf->setPaper('A4', 'portrait');
			$dompdf->render();
			if ($stream) {
				$dompdf->stream($filename.".pdf", array("Attachment" => 0));
			} else {
				return $dompdf->output();
			}
		}else{
			redirect('schedule');
		}
	}
}