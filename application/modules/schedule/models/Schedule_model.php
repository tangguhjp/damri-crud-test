<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Schedule_model extends CI_Model
{
	public function __construct(){
		parent::__construct();
		$this->load->database();
    }

    public function get_schedule_data(){
		$query = "select a.id, a.seat, 
			DATE_FORMAT(a.tanggal_tiket, '%d %b %Y') tgl_jadwal,  
			CONCAT('Rp. ',FORMAT(a.harga,0)) hrg_tiket,
			CONCAT(DATE_FORMAT(a.tanggal_tiket, '%H:%i'), ' WIB') jam_berangkat,
			b.city_name kota_asal,
			c.city_name kota_tujuan
		from schedule a
		left join cities b on b.city_id = a.asal
		left join cities c on c.city_id = a.tujuan
		where active=true
		order by a.tanggal_tiket";
		return $this->db->query($query)->result();
    }

	public function get_cities(){
		$query = "select city_id id, city_name name from cities order by city_name";
		return $this->db->query($query)->result();
	}

	public function get_detail($id){
		$query = "SELECT *, 
			DATE_FORMAT(tanggal_tiket, '%d-%m-%Y') tgl, 
			DATE_FORMAT(tanggal_tiket, '%H:%i') jam 
			FROM schedule 
		WHERE id=$id";

		$schedule = $this->db->query($query);
		if($schedule->num_rows() > 0){
			$data = $schedule->row_array();
			return [
				'status' => true,
				'data' => $data
			]; 
		}else{
			return [
				'status' => false,
				'data' => []
			]; 
		}
	}
	
	public function add($data){
		list($day, $month, $year) = explode("-",$data['tanggal']);
		$jam = $data['jam'];

		$schedule = array(
			'asal' 		=> $data['asal'],
			'tujuan' 	=> $data['tujuan'],
			'harga' 	=> $data['harga'],
			'seat' 		=> $data['seat'],
			'tanggal_tiket' => "$year-$month-$day $jam:00"
		);

		$this->db->insert('schedule',$schedule);

		if ($this->db->affected_rows()) {
			return [
				'status' => true,
				'message' => 'Berhasil menambah jadwal baru'
			]; 
		} else {
			return [
				'status' => false,
				'message' => 'Gagal menambah jadwal baru'
			];
		}
	}

	public function update($data){
		$id = $data['id'];
		$query = "select * from schedule where id=$id";
		$schedule = $this->db->query($query);

		if($schedule->num_rows() > 0){
			$row = $schedule->row_array();
			list($day, $month, $year) = explode("-",$data['tanggal']);
			$jam = $data['jam'];

			$row['asal'] = $data['asal'];
			$row['tujuan'] = $data['tujuan'];
			$row['harga'] = $data['harga'];
			$row['seat'] = $data['seat'];
			$row['tanggal_tiket'] = "$year-$month-$day $jam:00";

			$this->db->where('id', $id);
			$this->db->update('schedule', $row);

			if ($this->db->affected_rows()) {
				return [
					'status' => true,
					'message' => 'Berhasil mengubah jadwal tiket'
				]; 
			}else{
				return [
					'status' => true,
					'message' => 'Tidak ada perubahan jadwal tiket'
				]; 
			}
		}

		return [
			'status' => false,
			'message' => 'Gagal mengubah jadwal tiket'
		]; 
	}

	public function delete($id){
		$this->db->where('id', $id);
    	$this->db->delete('schedule');

		if($this->db->affected_rows()) {
			return [
				'status' => true,
				'message' => 'Berhasil menghapus jadwal tiket'
			]; 
		}else{
			return [
				'status' => false,
				'message' => 'Tidak ada jadwal tiket yang terhapus'
			]; 
		}
	}
}