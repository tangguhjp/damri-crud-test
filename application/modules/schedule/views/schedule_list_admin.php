<!-- Content area -->

<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<div class="content">

<!-- Main charts -->
<div class="row">
    <div class="col-xl-12">

        <!-- Traffic sources -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title font-weight-bold">JADWAL PENJUALAN TIKET</h6>
                <div class="pull-right">
                    <button id="btn-add-shedule" class="btn btn-sm btn-primary rounded-round"><i class="fa fa-calendar-plus-o"></i> Tambah</button>
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-success rounded-round dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Export</button>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(106px, 36px, 0px);">
                            <a href="<?php echo base_url() ?>schedule/exportPdf" target="_blank" class="dropdown-item"><i class="fa fa-file-pdf-o"></i>Pdf (.pdf)</a>
                            <a href="<?php echo base_url() ?>schedule/exportExcel" target="_blank" class="dropdown-item"><i class="fa fa-file-excel-o"></i>Excel (.xlsx)</a>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="card-body py-0">
                
                <div class="row">
                    <div class="col-sm-12">
                        <table id="schedule-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr class="bg-primary text-center">
                                    <th width="5%">No.</th>
                                    <th>Asal</th>
                                    <th>Tujuan</th>
                                    <th>Tanggal</th>
                                    <th>Harga</th>
                                    <th class="text-center">Seat</th>
                                    <th>Jam Keberangkatan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /traffic sources -->

    </div>
</div>
<!-- /main charts -->

<div id="modal_schedule" class="modal fade" data-backdrop="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-semibold">FORM JADWAL TIKET</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="" id="form-schedule">
                <div class="modal-body row">
                        <div class="form-group col-sm-12">
                            <label>Kota Asal</label>
                            <select class="form-control cities" name="asal" required>
                                <option value="">Pilih Kota Asal</option>
                                <?php foreach($cities as $c){?>
                                    <option value="<?php echo $c->id?>"><?php echo $c->name?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Kota Tujuan</label>
                            <select class="form-control cities" name="tujuan" required>
                                <option value="">Pilih Kota Tujuan</option>
                                <?php foreach($cities as $c){?>
                                    <option value="<?php echo $c->id?>"><?php echo $c->name?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="col-form-label col-lg-2 pl-0 pt-0">Seat</label>
                            <div class="col-lg-10 p-0" style="max-width: 100%;">
                                <div class="input-group">
                                    <input type="text" name="seat" class="form-control" placeholder="1A" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="col-form-label col-lg-2 pl-0 pt-0">Harga</label>
                            <div class="col-lg-10 p-0" style="max-width: 100%;">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </span>
                                    <input type="number" name="harga" class="form-control" placeholder="500000" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Tanggal</label>
                            <div class="input-group">
                                <input type="text" id="tanggal" name="tanggal" data-date-format="dd-mm-yyyy" class="form-control" value="<?= date('d-m-Y') ?>" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Jam</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="jam" name="jam" value="<?= date('H:i') ?>" readonly="" required>
                            </div>
                        </div>
                    <hr>
                    <p class="col-md-12">*) Isilah seluruh field dan pastikan yang anda input telah benar.</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn-submit-form" class="btn bg-primary">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>

</div>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/anytime.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.time.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/notifications/jgrowl.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/demo_pages/picker_date.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/modules/schedule_list_admin_view.js"></script>

<!-- /content area -->