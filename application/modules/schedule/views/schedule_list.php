<!-- Content area -->

<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">

<div class="content">

<!-- Main charts -->
<div class="row">
    <div class="col-xl-12">

        <!-- Traffic sources -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title font-weight-bold">JADWAL PENJUALAN TIKET</h6>
            </div>

            <div class="card-body py-0">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="schedule-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr class="bg-primary text-center">
                                    <th width="5%">No.</th>
                                    <th>Asal</th>
                                    <th>Tujuan</th>
                                    <th>Tanggal</th>
                                    <th>Harga</th>
                                    <th class="text-center">Seat</th>
                                    <th>Jam Keberangkatan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /traffic sources -->

    </div>
</div>
<!-- /main charts -->

</div>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/anytime.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.time.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/notifications/jgrowl.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/demo_pages/picker_date.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/modules/schedule_list_view.js"></script>

<!-- /content area -->