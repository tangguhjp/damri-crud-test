<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                font-size: 12px;
                text-align: center;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            th{
                text-align: center;
                background-color: #0558a4;
                color: white;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }

            .text-center{
                text-align: center;
            }
        </style>
    </head>
<body>
<img src="https://damri.co.id//assets/images/logo-damri.png" width="100px" alt="">
<h3 class="text-center">JADWAL PENJUALAN TIKET</h3>

<table>
  <tr>
    <th>No.</th>
    <th>Kota Asal</th>
    <th>Kota Tujuan</th>
    <th>Tanggal</th>
    <th>Harga</th>
    <th>Seat</th>
    <th>Jam Keberangkatan</th>
  </tr>
  <?php foreach($schedules as $key => $value){ ?>
    <tr>
            <td class="text-center"><?= ($key+1) ?></td>
            <td><?= $value->kota_asal ?></td>
            <td><?= $value->kota_tujuan ?></td>
            <td class="text-center"><?= $value->tgl_jadwal ?></td>
            <td class="text-center"><?= $value->hrg_tiket ?></td>
            <td class="text-center"><?= $value->seat ?></td>
            <td class="text-center"><?= $value->jam_berangkat ?></td>
    </tr>
  <?php } ?>
</table>

</body>
</html>

